"""
“Copyright 2019 La Coordinadora d’Entitats per la Lleialtat Santsenca”

This file is part of GNGforms.

GNGforms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from flask import Flask
from flask_pymongo import PyMongo
from flask_babel import Babel


app = Flask(__name__)
app.config.from_pyfile('config.cfg')
mongo = PyMongo(app)
babel = Babel(app)

app.config['RESERVED_SLUGS'] = ['admin', 'user', 'users', 'form', 'forms', 'site']
app.config['RESERVED_FORM_ELEMENT_NAMES'] = ['created']
app.config['LANGUAGES'] = {
    'en': 'English',
    'ca': 'Català',
    'es': 'Castellano'
}

from GNGforms import views

if __name__ == '__main__':
        app.run()
