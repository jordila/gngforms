��    a      $  �   ,      8     9     Q     n     u     �     �     �     �     �     �      �     �      	     	     	     )	  	   .	     8	     >	     L	     k	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     -
  	   6
     @
  
   I
     T
     [
     m
     v
     {
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          "     A     I     P     a  	   p     z     �     �     �     �  
   �     �     �     �     �     �                    "     &     6     C     P     ]     f     �     �     �     �     �     �     �       -        M  
   Q     \     p     �     �  �  �     Q     q     �     �     �     �     �     �      �       )   %     O     V     ]     }     �     �     �     �     �     �     �               "  "   :     ]     i     n     w     �     �     �     �     �  
   �     �     �               -     =     D     a     r     �     �     �     �     �     �     �  +   �     +  &   I     p     ~     �     �     �     �     �  *   �               1     ?     L     c     r     y  %   �     �     �     �  -   �     �     
          7  (   I  "   r  $   �     �     �     �  &   �       "   1  -   T     �     �     �     �     �     �     5          &       I   =   X               E   )         a   D   1   <          G       #   /         C   R   \       ?      U          (      %                  -       F   .   ^   0   >       "   	          O              Y       Q   +      !   ,      @   V         6                 L      9                     Z   8              `   M   B   ]   ;   :   3      A   2      N   '       7   *       T              [          $   _   J   W   4         S   H   P           
   K                All fields are required Another eg: My dog has fleas Author Bad credentials Can publish Can publish/login? Cancel Change Change email address Change password Could not connect to SMTP server Created Delete Delete form and data Download CSV Edit Edit form Email Email address Email address is not available Email address is not valid Entries False Form URL Form details Form is not available. 404 Forms Hello Hostname Include message Invitation not found Invitation only Invitations Invite a new user Is Admin Is public Language Last entry Logout My admin settings My forms Name New email address New form New user New user's email No No form found Not available! Nº of entries Password Password again Password changed OK Passwords do not match Permission needed to view form Preview Public Recover password Return to form Root user SMTP config works! Save Saved form OK Send Send a new user invitation Send email Settings Site settings Source code Start Test SMTP config. Text saved OK The form True URL Updated form OK Use this URL User details User's forms Username Username is not available Username is not valid Username not found Users Validated email View form data We may have sent you an email We sent an email to %s We sent an invitation to %s We will send you an email to your new address Yes Your email Your email is valid Your password is weak eg: No holidays this year user disabled Project-Id-Version: GNGforms 1.0
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-07-04 19:26+0200
PO-Revision-Date: 2019-07-04 19:34+0200
Last-Translator: 
Language: es
Language-Team: Lleialtec <chris@tec.lleialtat.cat>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
X-Generator: Poedit 1.8.11
 Todos los campos son necesarios Otro ej: Me perro tiene pulgas Autor Las credenciales no son buenas Puede publicar ¿Puede publicar/logear? Cancelar Cambiar Cambia la dirección de correo-e Cambia tu contraseña No se puede conectar con el servidor SMTP Creado Borrar Borrar el formulario y entradas Bajar el CSV Editar Editar formulario Correo-e Dirección de correo-e No está disponible el correo-e El correo-e no es válido Entradas Falso El URL del formulario Detalles del formulario No se encuentra el formulario. 404 Formularios Hola Hostname Incluir un mensaje No se encuentra la invitación Solo invitaciones Invitaciones Invita a un nuevo usuario Es Admin Es públic Idioma Última entrada Salir Mis preferencias de admin Mis formularios Nombre Nueva dirección de correo-e Nuevo formulario Usuario nuevo El correo-e del usuario nuevo No No se encuentra el formulario No disponible! Nº de entradas Contraseña Contraseña de nuevo Se ha cambiado la contraseña correctamente No coinciden las contraseñas Faltan permisos para ver el forumlario Previsualizar Público Recupera la contraseña Volver al formulario Usuario root La config. del SMTP funciona! Guardar Se ha guardado el formulario correctamente Enviar Invita un usuario nuevo Enviar correo Preferencias Preferencias del Sitio Código fuente Inicio Prueba la config. SMTP Se ha guardado el texto correctamente El formulario Verdad URL Se ha actualizado el formulario correctamente Usa este URL Detalles del usuario Formularios del usuario Nombre de usuario No está disponible el nombre de usuario El nombre de usuario no es válido No se encuentra el nombre de usuario Usuarios Correo-e validado Ver data del formulario Puede que te hemos enviado un correo-e Hemos enviado un correo a %s Hemos enviado una invitación a %s Te enviaremos un correo a tu dirección nueva Sí Tu correo-e Tu correo-e es válido Tu contraseña es débil ej. No hay vacaciones este año usuario deshabilitado 